# node api example

This project uses lot of stuff as:

- [TypeScript](https://www.typescriptlang.org/)
- [Jest](https://jestjs.io/)
- [Supertest](https://visionmedia.github.io/superagent/)
- [Eslint](https://eslint.org/)
- [Express](http://expressjs.com/)
- [Husky](https://github.com/typicode/husky)

## Getting Started

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Run tests:

```bash
npm run test
```

## Commands

- `dev`: runs your application on `localhost:3000`
- `test`: runs jest to test all components and pages
