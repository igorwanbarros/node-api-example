import { Router } from "express";
import TemplateController from "../controllers/templates";

const router: Router = Router();
const controller: TemplateController = new TemplateController();

router.get('/', controller.getAll);

router.get('/:id', controller.findById);

router.post('/', controller.create);

router.put('/:id', controller.update);

router.delete('/:id', controller.delete);

export default router;
