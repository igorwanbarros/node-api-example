import req, { Response } from "supertest";
import { Template, TemplateEntity } from "../models";
import app from "../app";
import pool from "../config/database";


beforeAll(async () => {
    const model = new Template();
    model.testInitialize();
});
afterAll(async () => {
    pool.end().then(() => console.log('closed database'));
});

test("test_get_templates", async () => {
    const template = new TemplateEntity();
    const model = new Template();

    template.name = 'template test';
    template.html = '<p>not found</p>';
    template.content = 'not found';
    template.language = 'pt-br';

    await model.create(template);
    await model.create(template);

    const res:Response = await req(app).get('/templates');
    
    expect(res.statusCode).toBe(200);
    expect(res.body.data.length).toEqual(2);
});


test("test_get_template_by_id", async () => {
    const template = new TemplateEntity();
    const model = new Template();

    template.name = 'template test by id';
    template.html = '<p>lorem ipsum</p>';
    template.content = 'lorem ipsum';
    template.language = 'pt-br';

    const result = await model.create(template);
    const res:Response = await req(app).get(`/templates/${result.id}`);

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(result);
});

test("test_create_template", async () => {
    const res:Response = await req(app)
        .post('/templates')
        .send({
            name: 'Template Test',
            html: '<h1>Hello Word</h1>',
            content: 'hello word',
            language: 'pt-br'
        });

    expect(res.status).toEqual(200);
    expect(res.body).toEqual(
        expect.objectContaining({name: 'Template Test'})
    );
});

test("test_update_template", async () => {
    const template = new TemplateEntity();
    const model = new Template();

    template.name = 'template test by update';
    template.html = '<p>lorem ipsum</p>';
    template.content = 'lorem ipsum';
    template.language = 'pt-br';

    const result = await model.create(template);

    const res:Response = await req(app)
        .put(`/templates/${result.id}`)
        .send({
            name: 'Template 2 Test',
            language: 'en-us'
        });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(
        expect.objectContaining({ id: result.id, name: 'Template 2 Test' })
    );
});

test("test_delete_template", async () => {
    const template = new TemplateEntity();
    const model = new Template();

    template.name = 'template test from delete';
    template.html = '<p>lorem ipsum</p>';
    template.content = 'lorem ipsum';
    template.language = 'pt-br';

    const result = await model.create(template);

    const res:Response = await req(app)
        .delete(`/templates/${result.id}`);

    expect(res.statusCode).toEqual(200);
});
