import express from "express";
import cors from "cors";

import templates from "./routes/templates";

const app: express.Application = express();

//permitir post data in json
app.use(express.json());

//permitir form-urlencoded
app.use(express.urlencoded({ extended: true }));

//add cors
app.use(cors());

//add routes to Template
app.use('/templates', templates);

//router test
app.get("/", (_, res) => {
  res.send({ message: "Hello ts-node!" });
});

export default app;
