import { plainToClass } from 'class-transformer';
import pool from '../config/database';

export interface iTemplate {
    id: number | null,
    name: string,
    html: string,
    content: string,
    language: string
};

export class TemplateEntity implements iTemplate {
    id = null;
    name = '';
    html = '';
    content = '';
    language = '';
    // TODO: add construct...
}

export class Template {

    public async all(): Promise<TemplateEntity[]> {
        const sql = "SELECT * FROM templates";
        const { rows } = await pool.query(sql);

        return plainToClass(TemplateEntity, rows);
    }

    public async findById(id :number): Promise<TemplateEntity> {
        const sql = "SELECT * FROM templates WHERE id = $1";
        const { rows } = await pool.query(sql, [id]);

        return plainToClass(TemplateEntity, rows[0]);
    }

    public async findBy(property: string, value: string, operator: string = '='): Promise<TemplateEntity[]> {
        const sql = `SELECT * FROM templates WHERE ${property} ${operator} $1`;
        const { rows } = await pool.query(sql, [value]);
        
        return plainToClass(TemplateEntity, rows);
    }

    public async create(template: TemplateEntity): Promise<TemplateEntity> {
        const { name, html, content, language } = template;
        const sql = "INSERT INTO templates(name, html, content, language) " + 
                    "VALUES($1, $2, $3, $4) RETURNING id";
        const { rows } = await pool.query(sql, [name, html, content, language]);
        
        return await this.findById(rows[0].id);
    }

    public async update(id: number, cols: TemplateEntity): Promise<TemplateEntity> {
        let update:string[] = [];
        let params:string[] = [];
        let index = 1;

        Object.entries(cols).forEach((item) => {
            const [key, value] = item;
            
            if (!value || value.length < 1) return;

            update.push(`${key} = $${index}`);
            params.push(value);
            index++;
        });

        const sql = `UPDATE templates SET ${update.join(',')} WHERE id = ${id}`;
        await pool.query(sql, params);

        return await this.findById(id);
    }

    public async delete(id: number): Promise<boolean> {
        const sql = "DELETE FROM templates WHERE id = $1";
        await pool.query(sql, [id]);

        return true;
    }

    public testInitialize() {
        const sql = "TRUNCATE templates RESTART IDENTITY";
        pool.query(sql);
    }
};
