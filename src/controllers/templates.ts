import { Request, Response } from "express";
import { TemplateEntity } from "../models";
import { TemplateService } from "../services/TemplateService";

const service = new TemplateService;

class TemplateController {

    public async getAll(_: Request, response: Response) {
        try {
            const data = await service.get();
            
            response.send({ data });
        } catch (error) {
            response.status(400).send(error);
        }
    }

    public async findById(request: Request, response: Response) {
        try {
            const { id } = request.params;
            const data = await service.find(+id);
            
            response.send(data);
        } catch (error) {
            console.log(error);
            response.status(400).send(error);
        }
    }

    public async create(request: Request, response: Response) {
        try {
            const template = <TemplateEntity>{...request.body};
            
            const data = await service.create(template);
            
            response.send(data);
        } catch (error) {
            response.status(400).send(error);
        }
    }

    public async update(request: Request, response: Response) {
        try {
            const { id } = request.params;
            const template = <TemplateEntity>{ ...request.body };
            
            const data = await service.update(+id, template);
            
            response.send(data);
        } catch (error) {
            response.status(500).send(error);
        }
    }

    public async delete(request: Request, response: Response) {
        try {
            const { id } = request.params;
            await service.delete(+id);
            
            response.send();
        } catch (error) {
            console.log(error);
            response.status(400).send(error);
        }
    }
}

export default TemplateController;
