import { Pool } from "pg";
import dotenv from "dotenv";

dotenv.config();

const pool = new Pool ({
    max: 20,
    connectionString: process.env.DB_CONNECTION,
    idleTimeoutMillis: 30000
});

pool.connect();

export default pool;
