import { Template, TemplateEntity } from '../models'


export class TemplateService {
    
    public async get(): Promise<TemplateEntity[]> {
        try {
            const model = new Template();

            return model.all();
        } catch (error) {
            throw new Error('On erro ocurred when connect database.');
        }
    }

    public async find(id: number): Promise<TemplateEntity> {
        try {
            const model = new Template();

            return await model.findById(id);
        } catch (error) {
            throw new Error('On erro ocurred when connect database.');
        }
    }

    public async findByName(name: string): Promise<TemplateEntity[]> {
        try {
            const model = new Template();

            return await model.findBy('name', name);
        } catch (error) {
            throw new Error('On erro ocurred when connect database.');
        }
    }

    public async create(data: TemplateEntity): Promise<TemplateEntity> {
        try {
            // TODO: add validation...
            const model = new Template();
            return await model.create(data);
        } catch (error) {
            throw new Error('On erro ocurred when create entity.');
        }
    }

    public async update(id: number, data:TemplateEntity): Promise<TemplateEntity> {
        try {
            // TODO: add validation...
            const model = new Template();
            return await model.update(id, data);
        } catch (error) {
            throw new Error('On erro ocurred when updated entity.');
        }
    }

    public async delete(id: number): Promise<boolean> {
        try {
            const model = new Template();
            const template = await model.findById(id);

            if (template.id === null) {
                throw new Error('Template not found.');
            }
            await model.delete(id);

            return true;
        } catch (error) {
            throw new Error('On erro ocurred when updated entity.');
        }
    }
}
